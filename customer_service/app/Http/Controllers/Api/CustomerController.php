<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers;
use Illuminate\Http\Request;
use App\Customer;
use Validator;
class CustomerController extends Controllers\Controller
{

    public function show($customer)
    {
        $code='200';
        $message='Query Executed Successfully';
        $data = Customer::where('id',$customer)->get();

        if ($data->isEmpty())
        {
        $data=[];
        $message='Record Not Found!';
        }
        else
        {
        $data->makeHidden(['updated_at','deleted_at']);
        }
        $respo= ['STATUS' => $code,
        'MESSAGE' => $message,
        'DATA'=>$data];
        return $respo;
    }


    public function index()
    {
        $data = Customer::all();
        $code='200';
        $message='Query Executed Successfully';
        $respo= ['STATUS' => $code,
        'MESSAGE' => $message,
        'DATA'=>$data];
        return $respo;
    }

    public function store(Request $request)
    {

        $code='201';
        $message=' Customer Created !';


        $validator = Validator::make($request->all(), [
            'full_name'=>'required',
            'telp'=>'required|digits_between:9,14|unique:customers',
            'email'=>'required|email|unique:customers',
        ]);

        if ($validator->fails()) {
            $errormsg= $validator->getMessageBag()->first();
         
            $code='422';  
            $respo= ['STATUS' => $code,
        'MESSAGE' => $errormsg];
        return $respo;
        }

            $customer=Customer::create($request->all());
            $respo= ['STATUS' => $code,
            'MESSAGE' => $message,
            'DATA'=>$customer];
            return $respo;

    }

    public function update(Request $request,$customer)
    {

        $code='200';
        $message='Customer Updated ! ';

        $validator = Validator::make($request->all(), [
            'full_name'=>'',
            'telp'=>'digits_between:9,14|unique:customers,telp,' . $customer,
            'email'=>'email|unique:customers,email,' . $customer,
        ]);

        if ($validator->fails()) {
           
            $code='422';
        $errormsg= $validator->getMessageBag()->first();
        $respo= ['STATUS' => $code,
            'MESSAGE' => $errormsg];
            return $respo;
        }

            $data=Customer::where('id',$customer)->first(); 

            if (is_null($data)){
            $data=[];
            $code='202';
            $message='Customer id not Found!';
            }
            else
            {
            $data->makeHidden(['updated_at']);
            $data->update($request->all());
            }
            $respo= ['STATUS' => $code,
            'MESSAGE' => $message,
            'DATA'=>$data];
            return $respo;     
    
    }



    public function destroy($customer)
    {

        $code='203';
        $message=$customer.' - Customer Deleted ! ';
        $data = Customer::where('id',$customer)->first();
        if (is_null($data))
        {
        $code='202';
        $message='Customer id not Found!';
        }
        else
        {
        $data->delete();     
        }
        $res['STATUS']=$code;
        $res['MESSAGE']=$message;  
        return $res;

    }

}
