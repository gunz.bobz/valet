<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes;
    protected $connection='mysql2';
    protected $table ='Bookings';

    protected $guarded=[];
    protected $hidden=['created_at','deleted_at','updated_at'];

}
