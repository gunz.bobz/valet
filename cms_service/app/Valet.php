<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Valet extends Model
{
    use SoftDeletes;
    protected $table ='valet_list';
    protected $guarded=[];
    protected $hidden=['created_at','deleted_at','updated_at'];

}
