<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->app->version();
});

//booking
$app->post('/booking/create', 'Api\v1\Booking\BookingController@store');
$app->get('/booking/{booking}', 'Api\v1\Booking\BookingController@show');
$app->put('/booking/{booking}/cancel', 'Api\v1\Booking\BookingController@cancel');
$app->put('/checkout/{booking}', 'Api\v1\Booking\BookingController@checkout');
$app->get('/booking/parked/today', 'Api\v1\Booking\BookingController@parkedtoday');
$app->get('/booking/canceled/today', 'Api\v1\Booking\BookingController@canceledtoday');
$app->get('/booking/completed/today', 'Api\v1\Booking\BookingController@completedtoday');
$app->get('/booking/waiting/today', 'Api\v1\Booking\BookingController@waitingtoday');

// customer
$app->post('/customer/create', 'Api\v1\Customer\CustomerController@store');
$app->get('/customer/{customer}', 'Api\v1\Customer\CustomerController@show');
$app->put('/customer/{customer}', 'Api\v1\Customer\CustomerController@update');
$app->delete('/customer/{customer}', 'Api\v1\Customer\CustomerController@delete');

// inquery 

$app->post('/booking/scan', 'Api\v1\Inquery\InqueryController@scanbarcode');
$app->post('/inquery/store', 'Api\v1\Inquery\InqueryController@store');

//cms capacity
$app->get('/current_capacity', 'Api\v1\Cms\CmsController@show');
$app->get('/used_valet', 'Api\v1\Cms\CmsController@showused');

//HISTORY
$app->get('/history/{history}', 'Api\v1\History\HistoryController@show');

//Splash
$app->get('/splashes', 'Api\v1\Splash\SplashController@index');
$app->get('/splashes/{splash}', 'Api\v1\Splash\SplashController@show');
$app->delete('/splashes/{splash}', 'Api\v1\Splash\SplashController@delete');