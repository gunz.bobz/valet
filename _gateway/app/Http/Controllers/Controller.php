<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use GuzzleHttp\Client;
class Controller extends BaseController
{
    protected function Get($url)
    {
        $client = New Client();
        $reqGet = $client->get($url);
        $resGet = json_decode($reqGet->getBody(),true);
        return $resGet;
    }
    protected function Post($url,$data)
    {
        $client = new Client();
        $reqPost = $client->post($url,[
            'form_params'=>$data
        ]);
        $resPost = json_decode($reqPost->getBody(),true);
        return $resPost;
    }
    protected function Post2($url,$data)
    {
        $client = new Client();
        $reqPost = $client->post($url,[
            'multipart'=>$data
        ]);
        $resPost = json_decode($reqPost->getBody(),true);
        return $resPost;
    }



}
