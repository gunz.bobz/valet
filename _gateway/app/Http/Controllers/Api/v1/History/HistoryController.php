<?php

namespace App\Http\Controllers\Api\v1\History;

use App\Http\Controllers\Controller;
use App\Services\HistoryService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;


class HistoryController extends Controller
{
   
    /**
     * The service to consume the history micro-service
     * @var HistoryService
     */


 

   

    /**
     * Show a single history details
     * @param $history
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($history)
    {
        return  $this->Get(env('HISTORY_SERVICE_URL').'/api/history/'.$history);
    }
   


}
