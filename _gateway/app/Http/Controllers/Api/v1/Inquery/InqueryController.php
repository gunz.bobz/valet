<?php

namespace App\Http\Controllers\Api\v1\Inquery;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use GuzzleHttp\Client;


class InqueryController extends Controller
{

    /**
     * The service to consume the inquery micro-service
     * @var InqueryService
     */




    /**
     * Show a single inquery details
     * @param $inquery
     * @return \Illuminate\Http\JsonResponse
     */
    public function scanbarcode(Request $request)


    {
        // $data=$request->barcode;
        // $params = [
        //     'form_params' => [
        //        'barcode' => $data
               
        //     ]
        //  ];



        $data['barcode']=$request->barcode;
    // $data=$request->all();
    
    
    // dd($data);
    return  $this->Post(env('BOOKING_SERVICE_URL').'/api/booking/scan', $data);
    
    
    
    // $client = new Client();
    
    // $response= $client->request('POST',env('BOOKING_SERVICE_URL').'/api/booking/scan',$params);
    // return json_decode($response->getBody(),true);
    
}
public function store(Request $request)
{
    $data=$request->all();
    // dd($data);
    // return $data;
    return $this->Post(env('INQUERY_SERVICE_URL').'/api/inquery/store',$data);
    
}
    public function delete($inquery)
    {
     
        
        $client = new Client();

        $response= $client->request('DELETE',env('INQUERY_SERVICE_URL').'/api/inquery/'.$inquery);
        return json_decode($response->getBody(),true);
    }






}
