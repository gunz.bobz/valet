<?php

namespace App\Http\Controllers\Api\v1\Cms;

use App\Http\Controllers\Controller;
use App\Services\CmsService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;


class CmsController extends Controller
{
   
    /**
     * The service to consume the cms micro-service
     * @var CmsService
     */


 

   

    /**
     * Show a single cms details
     * @param $cms
     * @return \Illuminate\Http\JsonResponse
     */
    public function show()
    {
        return  $this->Get(env('CMS_SERVICE_URL').'/api/current_capacity');
    }
    public function showused()
    {
        return  $this->Get(env('CMS_SERVICE_URL').'/api/used_valet');
    }

    public function store(Request $request)
    {
        $data=$request->all();
        // return $data;
        return $this->Post(env('CMS_SERVICE_URL').'/api/cms/create',$data);
        
    }



}
