<?php

namespace App\Http\Controllers\Api\v1\Splash;

use App\Http\Controllers\Controller;
use App\Services\SplashService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;


class SplashController extends Controller
{
   
    /**
     * The service to consume the splash micro-service
     * @var SplashService
     */


 

   

    /**
     * Show a single splash details
     * @param $splash
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($splash)
    {
        return  $this->Get(env('SPLASH_SERVICE_URL').'/api/splashes/'.$splash);
    }
    public function index()
    {
        return  $this->Get(env('SPLASH_SERVICE_URL').'/api/splashes');
    }

    public function delete($splash)
    {
     
        
        $client = new Client();

        $response= $client->request('DELETE',env('SPLASH_SERVICE_URL').'/api/splashes/'.$splash);
        return json_decode($response->getBody(),true);
    }

}
