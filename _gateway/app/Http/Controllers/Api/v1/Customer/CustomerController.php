<?php

namespace App\Http\Controllers\Api\v1\Customer;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;


class CustomerController extends Controller
{

    /**
     * The service to consume the customer micro-service
     * @var CustomerService
     */




    /**
     * Show a single customer details
     * @param $customer
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($customer)
    {
        return  $this->Get(env('CUSTOMER_SERVICE_URL').'/api/customer/'.$customer);
    }
    public function delete($customer)
    {
    
        
        $client = new Client();

        $response= $client->request('DELETE',env('CUSTOMER_SERVICE_URL').'/api/customer/'.$customer);
        return json_decode($response->getBody(),true);
    }


    public function store(Request $request)
    {
        $data=$request->all();
        // return $data;
        return $this->Post(env('CUSTOMER_SERVICE_URL').'/api/customer/create',$data);
        
    }

    public function update(Request $request,$customer)
    {
        $data=$request->all();
        $client = new Client();

        $response= $client->request('PUT',env('CUSTOMER_SERVICE_URL').'/api/customer/'.$customer,['form_params' => $data]);
        return json_decode($response->getBody(),true);
    }


}
