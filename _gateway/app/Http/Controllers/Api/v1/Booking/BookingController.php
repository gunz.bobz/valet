<?php

namespace App\Http\Controllers\Api\v1\Booking;

use App\Http\Controllers\Controller;
use App\Services\BookingService;
use Illuminate\Http\Request;

use GuzzleHttp\Client;


class BookingController extends Controller
{
   
    /**
     * The service to consume the booking micro-service
     * @var BookingService
     */


 

   

    /**
     * Show a single booking details
     * @param $booking
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($booking)
    {
        return  $this->Get(env('BOOKING_SERVICE_URL').'/api/booking/'.$booking);
    }
    public function parkedtoday()
    {
        return  $this->Get(env('BOOKING_SERVICE_URL').'/api/booking/parked/today');
    }
    public function canceledtoday()
    {
        return  $this->Get(env('BOOKING_SERVICE_URL').'/api/booking/canceled/today');
    }
    public function completedtoday()
    {
        return  $this->Get(env('BOOKING_SERVICE_URL').'/api/booking/completed/today');
    }
    public function waitingtoday()
    {
        return  $this->Get(env('BOOKING_SERVICE_URL').'/api/booking/waiting/today');
    }
   

    public function store(Request $request)
    {
        $data=$request->all();
        // return $data;
        return $this->Post(env('BOOKING_SERVICE_URL').'/api/booking/create',$data);
        
    }

    public function barcodescan($barcode)
    {
    
        $client = new Client();
        $response= $client->request('PUT',env('INQUERIES_SERVICE_BASE_URL').'/booking/'.$barcode);
        return json_decode($response->getBody(),true);
        // return $this->Post(env('INQUERIES_SERVICE_BASE_URL').'/booking/',$data);
    }

    public function cancel($barcode)
    {
    
        $client = new Client();
        $response= $client->request('PUT',env('BOOKING_SERVICE_URL').'/api/booking/'.$barcode);
        return json_decode($response->getBody(),true);
        // return $this->Post(env('INQUERIES_SERVICE_BASE_URL').'/booking/',$barcode.'/cancel');
    }
    public function checkout($barcode)
    {
    
        $client = new Client();
        $response= $client->request('PUT',env('BOOKING_SERVICE_URL').'/api/checkout/'.$barcode);
        return json_decode($response->getBody(),true);
        // return $this->Post(env('INQUERIES_SERVICE_BASE_URL').'/booking/',$barcode.'/cancel');
    }


}
