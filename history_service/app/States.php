<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class States extends Model
{
    
    protected $fillable=[];
    protected $connection='booking';
    protected $hidden=['created_at','deleted_at','updated_at'];
    
    
}
