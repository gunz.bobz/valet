<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inquery extends Model
{
    use SoftDeletes;
    protected $guarded=[];

    // protected $appends = ['null_date'];
    protected $hidden = ['deleted_at','created_at','updated_at'];
    // public function getNullDateAttribute()
    // {
    //     if ($this->attributes['deleted_at'] == 'da'){
    //     return $this->attributes['deleted_at'] == '';
    // }

    // public function history()
    // {
    //     return $this->hasMany('App\Status', 'id', 'status');
    // }
    public function valet()
    {
        return $this->hasMany('App\Booking', 'barcode', 'barcode');
    }     
    public function getstatus()
    {
        return $this->hasOne('App\States', 'id', 'status_id');
    }    
    public function getnama()
    {
        return $this->hasOne('App\Customer', 'customer_id', 'id_customer');
    }     
   
}
