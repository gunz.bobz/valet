<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes;
    protected $connection='booking';
    protected $guarded=[];
    protected $hidden=['deleted_at','updated_at'];
    public function getnama()
    {
        return $this->hasOne('App\Customer', 'id', 'customer_id');
    }   
    public function getstatus()
    {
        return $this->hasOne('App\States', 'id', 'state');
    }     
    
}
