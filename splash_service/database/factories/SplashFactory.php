<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Splash;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Splash::class, function (Faker $faker) {
    return [


        'image' => $faker->imageUrl($width = 640, $height = 480),
        'title' => $faker->sentence($nbWords = 6, $variableNbWords = true) ,
        'subtitle' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
        'desc' => $faker->text($maxNbChars = 200),
        'created_by' => $faker-> randomElement(['1', '2']),
        'updated_by' =>$faker-> randomElement(['1', '2']),
    ];
});
