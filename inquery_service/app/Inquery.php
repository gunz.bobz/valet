<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inquery extends Model
{
    use SoftDeletes;
    protected $guarded=[];
    protected $hidden=['updated_at','deleted_at','id'];
    public function getstatus()
    {
        return $this->hasOne('App\State', 'id', 'state');
    }    
    public function getnama()
    {
        return $this->hasOne('App\Customer', 'id', 'customer_id');
    }    
}
