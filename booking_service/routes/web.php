<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/key', function() {
    return str_random(32);
});

$app->post('api/booking/create','Api\BookingController@store');
$app->get('api/booking/{bookings}','Api\BookingController@show');
$app->post('api/booking/scan','Api\BookingController@scan');
$app->put('api/booking/{bookings}','Api\BookingController@cancel');
$app->put('api/checkout/{bookings}','Api\BookingController@checkout');
$app->get('api/booking/parked/today','Api\BookingController@todayparked');
$app->get('api/booking/waiting/today','Api\BookingController@todaywaiting');
$app->get('api/booking/canceled/today','Api\BookingController@todaycanceled');
$app->get('api/booking/completed/today','Api\BookingController@todaycompleted');

