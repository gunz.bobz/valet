<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('states')->delete();
        
        \DB::table('states')->insert(array (
            0 => 
            array (
                'id' => '1',
                'state' => 'In Process',
            ),
            1 => 
            array (
                'id' => '2',
                'state' => 'Parked',
                
            ),
            2 => 
            array (
                'id' => '3',
                'state' => 'Completed',
                
            ),
            3 => 
            array (
                'id' => '4',
                'state' => 'Canceled',
                
            ),
            4 => 
            array (
                'id' => '5',
                'state' => 'other',
                
            ),
        ));
        
        
    }
}