<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValetBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
          
                $table->bigIncrements('id');
                $table->string('inquery_no',10)->nullable();
                $table->string('barcode',10)->nullable();
                $table->string('customer_id',10);
                $table->string('vehicle_brand',255);
                $table->string('vehicle_color',255);
                $table->string('vehicle_id',255);
                $table->string('state',255)->default('1');
                $table->timestamps();
                $table->dateTime('expired_at')->nullable();
                $table->softDeletes();
            });
        }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
