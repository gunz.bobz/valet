<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValetBookinsTriggers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER `generatenomorinquery` BEFORE INSERT ON `bookings` FOR EACH ROW SET NEW.expired_at = TIMESTAMPADD(MINUTE,60, NEW.created_at), NEW.inquery_no = lpad(concat(day(NEW.created_at),(SELECT count(*)+1 FROM bookings where date(created_at)=curdate())),4,0), NEW.barcode = concat(year(now()),lpad(month(now()),2,0),(SELECT LPAD(AUTO_INCREMENT,4,0) FROM `information_schema`.`TABLES` where TABLE_SCHEMA="valet_booking" and TABLE_NAME="bookings"))');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS `generatenomorinquery`');
        
    }
}
