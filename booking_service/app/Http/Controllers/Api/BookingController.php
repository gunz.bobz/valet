<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Booking;
use App\Inquery;
use Carbon\Carbon;
use Validator;

class BookingController extends Controller
{
    public function show($booking)
    {

        $code='200';
        $message='Record Found';
        
        $data = Booking::where('barcode',$booking)
        ->with('getState')
        ->with('getName')
        ->get();

        if ($data->isEmpty())
        {
        $data=[];
        $code='424';
        $message='Barcode NOT FOUND';
        }
        else
        {
        $data->makeHidden(['updated_at']);
        }


        $res= ['STATUS' => $code,
            'MESSAGE' => $message,
            'DATA' => $data];
        return $res;

    }

    public function todayparked()
    {
    
        $code='200';
        $message='Query Executed Successfuly';
        $data = Booking::where('state','2')->whereDate('created_at', Carbon::today())
        ->with('getState')->with('getName')->get();

        if ($data->isEmpty()){
        $data=[];
        $message='Record NOT FOUND';
        }

        else{
        $data->makeHidden(['updated_at']);
        }

        $res= ['STATUS' => $code,
        'MESSAGE' => $message,
        'DATA' => $data];
         return $res;
    }
    public function todaywaiting()
    {
        $code='200';
        $message='Query Executed Successfuly';
        $data = Booking::where('state','1')->whereDate('created_at', Carbon::today())
        ->with('getState')
        ->with('getName')
        ->get();
        if ($data->isEmpty()){
        $data=[];
        $message='Record NOT FOUND';
        }

        else{
        $data->makeHidden(['updated_at']);
        }

        $res= ['STATUS' => $code,
        'MESSAGE' => $message,
        'DATA' => $data];
    return $res;
    }
    public function todaycanceled()
    {
        $code='200';
        $message='Query Executed Successfuly';
        $data = Booking::where('state','4')->whereDate('created_at', Carbon::today())
        ->with('getState')->with('getName')->get();
        if ($data->isEmpty()){
        $data=[];
        $message='Record NOT FOUND';
        }
        
        else{
        $data->makeHidden(['updated_at']);
        }

        $res= ['STATUS' => $code,
        'MESSAGE' => $message,
        'DATA' => $data];
    return $res;

    }
    public function todaycompleted()
    {

        $code='200';
        $message='Query Executed Successfuly';
        $data = Booking::where('state','3')->whereDate('created_at', Carbon::today())
        ->with('getState')->with('getName')->get();
        if ($data->isEmpty()){
        $data=[];
        $message='Record NOT FOUND';
        }

        else{
        $data->makeHidden(['updated_at']);
        }
        $res= ['STATUS' => $code,
        'MESSAGE' => $message,
        'DATA' => $data];
         return $res;

    }

    public function scan(Request $request)
    {

        // return('dad');
        // return( $request);
    //    return ( $request->all());
        $test = $request['barcode'];
   
    // echo $test;
       
        $code='200';
        $message='Barcode is Valid !';
        $data = Booking::where('barcode',$test)
        ->where('state','1')
        ->with('getState')->with('getName')->get();
        if ($data->isEmpty()){
        $data=[];
        $code='202';
        $message='Invalid Barcode!';
        }

        else{
        $data->makeHidden(['updated_at']);
        }

        $res= ['STATUS' => $code,
        'MESSAGE' => $message,
        'DATA' => $data];
    return $res;
    }

    public function usedvalet(Request $request)
    {
        $data = Booking::where('status_id','2')->count();
        $res= ['STATUS' => '200',
        'MESSAGE' => 'OK',
        'DATA' => $data];
             return $res;
    }


    public function store(Request $request)
    {
        $code='200';
        $message='Valet Booked';
        $validator = Validator::make($request->all(), [
            'customer_id'=>'required',
            'vehicle_brand'=>'required',
            'vehicle_color'=>'required',
            'vehicle_id'=>'required',
        ]);
        if ($validator->fails()) {
            $code='422';
            $message= $validator->getMessageBag()->first();

            $res= ['STATUS' => $code,
                'MESSAGE' => $message];
        
                return $res;

        }

            $booking=Booking::create($request->all());
            $res= ['STATUS' => $code,
            'MESSAGE' => $message,
            'DATA'=>$booking];
    
            return $res;

    }

    public function checkout(Request $request,$booking)
    {      

            $data=Booking::where('barcode',$booking)
            ->where('state','2')
            ->first();
            $inquerydata=Inquery::where('barcode',$booking)->first();

            if ((is_null($data)) OR (is_null($inquerydata))){
            $code='422';
            $message='Invalid Barcode | State != 2 (PARKED)';
         
            $res= ['STATUS' => $code,
                    'MESSAGE' => $message];
    
            return $res;
            }

            else{
            $inquerydata->completed_at = Carbon::now();
            $inquerydata->save();
            $data->state = '3';
            $data->save();        
            $res=Booking::where('barcode',$booking)->with('getName')
            ->with('getState')->first();
            $code='200';
            $message='Checked Out Success';
            $respo= ['STATUS' => $code,
            'MESSAGE' => $message,
            'DATA'=>$res];
            return $respo;
            }          
    }
    
    public function cancel(Request $request,$booking)
    {
        $code='200';
        $message=$booking.' Booking is Canceled !';
            $data=Booking::where('barcode',$booking)->whereIn('state',['2','1'])->first();
            if (is_null($data)){
            $code='422';
            $message='Invalid Barcode | State must be 1 (waiting on confirm) or 2 (parked)';
           
            $res= ['STATUS' => $code,
                    'MESSAGE' => $message];
    
            return $res;
            }
            
            else{
            $data->state = '4';
            $data->save();        
            $res=Booking::where('barcode',$booking)
            ->with('getState')->with('getName')->get(); 
            $respo= ['STATUS' => $code,
            'MESSAGE' => $message,
            'DATA'=>$res];
            return $respo;
            }      
    }
    public function destroy($booking)
    {
        $data = Booking::where('id',$booking);
        $data->delete();
        return response()->json();        
    }
}
