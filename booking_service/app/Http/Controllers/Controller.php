<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function jsonresponse($status,$message,$data){

        return ['STATUS' => $status,
        'MESSAGE' => $message,
        'DATA' => $data];
    }
}
